#!/bin/bash
for branch in develop qa staging; 
    do 
      echo $branch
      git checkout $branch
      git merge --no-ff master
      git push origin $branch
      echo $branch "merge successful"
done

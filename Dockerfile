FROM ubuntu
# Install Nginx.
# Define mountable directories.
WORKDIR /etc/nginx
# Define default command.
CMD ["nginx"]
# Expose ports.
EXPOSE 80
EXPOSE 443

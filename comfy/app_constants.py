import os
import re

"""
Copyright 2015 Building Robotics, Inc.  All rights reserved.

@author Grant Patterson <grant@buildingrobotics.com>
"""

# Increment this to force all mobile app clients to upgrade.
APP_MAJOR = 2
# Apple requires a new minor number for each new iOS release.
APP_MINOR = 125
